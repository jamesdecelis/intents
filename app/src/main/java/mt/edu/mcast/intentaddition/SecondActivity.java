package mt.edu.mcast.intentaddition;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {

    final int THIRD_ACTIVITY_REQUEST_CODE = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

    }

    public void addClicked(View v){

        EditText etxtNum1 = findViewById(R.id.etxt_num1);
        EditText etxtNum2 = findViewById(R.id.etxt_num2);

        int num1 = Integer.parseInt(etxtNum1.getText().toString());
        int num2 = Integer.parseInt(etxtNum2.getText().toString());

        Intent i = new Intent(this, ThirdActivity.class);

        i.putExtra("num1", num1);
        i.putExtra("num2", num2);

        startActivityForResult(i, THIRD_ACTIVITY_REQUEST_CODE);

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data){

    }
}
