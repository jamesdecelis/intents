package mt.edu.mcast.intentaddition;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Bundle extras = getIntent().getExtras();

        if(extras != null){

            int num1 = extras.getInt("num1");
            int num2 = extras.getInt("num2");

            TextView tv = findViewById(R.id.tv_sum);
            tv.setText(num1 + " + " + num2 + " = " + (num1+num2));

        }

        Button btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
