package mt.edu.mcast.intentaddition;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    final int PICK_PHOTO_FOR_AVATAR = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public boolean onCreateOptionsMenu(Menu m){

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, m);
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){

            case R.id.mitem_add : {
                Intent i = new Intent(this, SecondActivity.class);
                startActivity(i);
                break;
            }
            case R.id.mitem_about: {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.mcast.edu.mt/"));
                startActivity(i);
                break;
            }
            case R.id.mitm_gallery: {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
                break;
            }
        }

        return true;

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
                try {
                    InputStream inputStream = getApplication().getContentResolver().openInputStream(data.getData());

                    ImageView iv = findViewById(R.id.imageView);
                    iv.setImageBitmap(BitmapFactory.decodeStream(inputStream));

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        }
    }
}
